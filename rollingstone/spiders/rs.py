# -*- coding: utf-8 -*-
import scrapy
import scrapy.loader

from rollingstone.items import PageLoader


class RsSpider(scrapy.Spider):
    name = "rs"
    allowed_domains = ["rollingstone.ru"]

    def start_requests(self):
        with open("input/urls_test.txt") as file:
            for url in file:
                url = url.strip()
                yield scrapy.Request(url)

    def parse(self, response):
        if self.page_is_broken(response):
            return

        loader = PageLoader(response=response)

        # Url
        loader.add_value("url_absolute", response.request.url)
        loader.add_value("url_relative", response.request.url, re="//.*?(/.*$)")
        loader.add_value("url_basename", response.request.url, re="//.*?/.*?([^/]+/?$)")
        loader.add_value("url_basename", "")  # for main page

        # Meta
        loader.add_xpath("meta_title", "normalize-space(//title)")
        loader.add_xpath("meta_h1", "normalize-space(//h1)")
        loader.add_xpath("meta_keywords", "normalize-space(//meta[@name='keywords']/@content)")
        loader.add_xpath("meta_description", "normalize-space(//meta[@name='description']/@content)")

        # Article title
        loader.add_xpath("title", "normalize-space(//h1)")

        # Article content
        # http://www.rollingstone.ru/articles/1997
        loader.add_css("content", "td.osntblleftb > div.imgleft")
        loader.add_css("content", "div.articleinner")
        loader.add_css("content", "div.block-lc div.block-inner")
        # http://www.rollingstone.ru/articles/cinema/article/12262.html
        loader.add_css("content", "div.block-img-osn")
        loader.add_css("content", "div.block-content")
        # http://www.rollingstone.ru/style/gallery/13-9.html
        loader.add_css("content", "div.photogallery-description")
        loader.add_css("content", "div.block-img-photogallery")
        loader.add_css("content", ".block-left > .block-phg-img-prew")
        loader.add_css("content", "div.block-photo-description")
        # http://www.rollingstone.ru/artists/artist/agytin_leonid_39/
        loader.add_css("content", ".full_item_div")
        loader.add_css("content", ".full_divider_div + .main_part_rss_div")
        loader.add_css("content", ".all_news_div")
        loader.add_css("content", ":not(.all_news_div) > .artist_add_info_block")
        loader.add_css("content", ":not(.all_news_div) > .artist_add_info_block + .links_par")
        # http://www.rollingstone.ru/articles/rubric/_/article/within_temptation_the_heart_of_everything_2106/
        loader.add_xpath("content", "//html", re='(?s)class="one_full_news_div.*?subhead_div.*?</div>(.*?)<div class="date_div')
        loader.add_xpath("content", "//html", re='(?s)<h1 class="ttlrubrica.*?</h1>(.*?)<div class="clear" style=')
        # http://www.rollingstone.ru/heroes/10
        loader.add_xpath("content", "//html", re='(?s)<h1 class="ttlrubrica.*?</h1>(.*?)<div class="clear" style=')

        # Article date
        loader.add_xpath("date", "normalize-space(//div[@class='block-data-author']/span[@class='red-bold'])")
        loader.add_xpath("date", "normalize-space(//td[@class='tdl']/div[@class='data'])")
        loader.add_xpath("date", "normalize-space(//div[@class='clear_div']/following-sibling::div[@class='date_div'])")

        # Article author
        loader.add_xpath("author", "normalize-space(//div[@class='block-data-author']/span[@class='black-bold'])")
        loader.add_xpath("author", "normalize-space(//em)", re="^Автор: (.*)$")
        loader.add_xpath("author", "normalize-space(//p[@class='author_name'])", re="^Автор: (.*)$")
        loader.add_css("author", ".tbl-auth-likes > tr > td.tdl > nobr > span::text")

        # Links
        loader.add_value("links", self.extract_links(response))

        # Images
        loader.add_xpath("image_urls", "//img[@src]/@src")

        return loader.load_item()

    @staticmethod
    def extract_links(response):
        import urllib.parse

        links = []
        for a in response.xpath("//a[@href]"):
            text = a.xpath("normalize-space(text())").get() or ""
            link = a.xpath("@href").get()
            link = link.split("?")[0].split("#")[0]
            if link == "":
                continue
            link = urllib.parse.urljoin("http://www.rollingstone.ru/", link)
            links.append((text, link))
        links = set(links)
        return links

    @staticmethod
    def page_is_broken(response):
        import re

        url = response.request.url
        title = response.css("title::text").get() or ""

        checks = (
            ("encoding", title == ""),
            ("wayback", title == "Wayback Machine"),
            ("redirect", response.status != 200),
            # ("redirect 200", re.match(r"^ROLLING STONE RUSSIA[^\w]*?(мужской )?журнал о современной", title)),
            ("not found", title.endswith("- Ничего не обнаружено")),
        )

        for reason, check in checks:
            if check:
                with open("output/urls_broken.csv", "a") as file:
                    file.write(f"{reason},{url}\n")
                return True

        return False
