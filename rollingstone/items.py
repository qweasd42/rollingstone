# -*- coding: utf-8 -*-
import re
import urllib.parse

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, Join


def default_output_processor(self, values):
    for value in values:
        if value is not None and value != "":
            return value
    else:
        return ""


def content_input_processor(self, values):
    for value in values:
        regexes = (
            r"<script.*?</script>",
            r"<h1.*?</h1>",
            r"<p><em>Автор:.*?>",
            r'<div class="data.*?</div>',
            r'<table class="tbl-root.*?</table>',
            r'<table class="tbl-auth-likes.*?</table>',
        )
        for regex in regexes:
            value = re.sub(regex, "", value)
        value = re.sub(r"\s{2,}", " ", value)
        value = value.strip()
        yield value


def image_urls_output_processor(self, values):
    for value in values:
        value = urllib.parse.urljoin("http://www.rollingstone.ru/", value)
        yield value


class Page(scrapy.Item):
    url_absolute = scrapy.Field()
    url_relative = scrapy.Field()
    url_basename = scrapy.Field()
    meta_title = scrapy.Field()
    meta_h1 = scrapy.Field()
    meta_keywords = scrapy.Field()
    meta_description = scrapy.Field()
    title = scrapy.Field()
    content = scrapy.Field()
    date = scrapy.Field()
    author = scrapy.Field()
    links = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()


class PageLoader(ItemLoader):
    default_item_class = Page
    default_output_processor = default_output_processor

    content_in = content_input_processor
    content_out = Join("")

    links_out = Identity()

    image_urls_out = image_urls_output_processor
