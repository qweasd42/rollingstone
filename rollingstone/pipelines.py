# -*- coding: utf-8 -*-
import json
import os
from urllib.parse import urlparse

from scrapy.pipelines.images import ImagesPipeline


class MyImagesPipeline(ImagesPipeline):
    def file_path(self, request, response=None, info=None):
        return "images/" + urlparse(request.url).path


class JsonWriterPipeline(object):
    items = []
    chunk_size = 500
    file_count = 1

    def close_spider(self, spider):
        if len(self.items):
            path = f"output/posts/{self.file_count:02d}.json"
            os.makedirs(os.path.dirname(path), exist_ok=True)
            with open(path, "w") as file:
                json.dump(self.items, file, ensure_ascii=False, indent=None)

    def process_item(self, item, spider):
        item_dict = dict(item)
        item_dict.pop("image_urls", None)
        item_dict.pop("images", None)
        self.items.append(item_dict)
        if len(self.items) >= self.chunk_size:
            path = f"output/posts/{self.file_count:02d}.json"
            os.makedirs(os.path.dirname(path), exist_ok=True)
            with open(path, "w") as file:
                json.dump(self.items, file, ensure_ascii=False, indent=None)
            self.items.clear()
            self.file_count += 1
        # return item
